#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""Symplectic and non-symplectic integrators. Plotting code adapted from a 
code by Steve Spicklemire
"""
from  __future__ import division, print_function
from numpy import *
import numpy
from matplotlib.pyplot import *
from pylab import *

def plotintegrate(stepper,fn):
  t = 0.0
  dt = pi/10

  delta=0.1 # how far apart in phase space are the points

  s0= array([1.0,0.0])
  s1=s0 + array([delta, 0])
  s2=s0 + array([delta,delta])
  s3=s0 + array([0, delta])  # five points in phase space
  s=array(list(flatten(array([s0,s1,s2,s3,s0]).T)))
  n = len(s)/2
  q = s[:n]  
  p = s[n:]



  rcParams['figure.figsize']=(7.,7.)
  clf()
  wsize=2.5
  axes().set_aspect('equal')
  axis([-wsize,wsize,-wsize,wsize])
  xlabel("position")
  ylabel("momentum")

  while t<2*pi:
      plot(q, p, 'r-',q,p,'b.')
      q,p = stepper(q,p, dt)
      t+=dt

  savefig(fn)


def getdelE(Nstepspercycle,Ncycles,stepper):
  # length of a cycle is 2pi
  dt = 2*pi / Nstepspercycle
  Nsteps = Nstepspercycle*Ncycles
  t = numpy.linspace(0 ,Nsteps*dt, Nsteps, endpoint=False)
  E = numpy.zeros(Nsteps)
  p = 0
  q = 1
  for i in xrange(Nsteps):
    # find the energy
    E[i] = 0.5*p**2 + 0.5*q**2 
    # update positions
    q,p = stepper (q,p,dt)

  delE = 1/Nsteps * numpy.abs((E-E[0])/E[0]).sum()
  return delE  

def plotdelE(Ncycles,stepper):
  Nstepspercyclearr = numpy.arange(4,300)
  delEarr = zeros(len(Nstepspercyclearr))
  for jj in range(len(Nstepspercyclearr)):  
    delE = getdelE(Nstepspercyclearr[jj], Ncycles,stepper  )
    delEarr[jj] = delE

  dtarr = 2*pi / Nstepspercyclearr

  # standard plot
  figure()
  plot(dtarr,delEarr,"ro")
  xlabel(r"$\delta t$")
  ylabel(r"$\Delta E$")
  savefig("plotdelE.eps")

  # log-log plot
  figure()
  plot(numpy.log(dtarr),numpy.log(delEarr),"ro")
  logdelEquadratic = 2*(numpy.log(dtarr)-numpy.log(dtarr)[0]) + numpy.log(delEarr)[0]
  plot(numpy.log(dtarr),logdelEquadratic)
  xlabel(r"$\log \left( \delta t \right) $")
  ylabel(r"$\log \left( \Delta E \right)$")
  savefig("plotdelE_log.eps")
    

def EulerStep(q,p, dt):
  qnew = q + p*dt
  pnew = p - q*dt
  return qnew,pnew

def SymplecticEulerStep(q,p, dt):
  qnew = q + p*dt
  pnew = p - qnew*dt
  return qnew,pnew

def VerletStep(q,p,dt):
  qnew = q + p*dt - q/2.*dt**2
  pnew = p + (-q-qnew)/2.*dt
  return qnew, pnew

plotdelE(10,VerletStep)


plotintegrate(EulerStep,"euler.eps")
plotintegrate(SymplecticEulerStep,"symeuler.eps")
plotintegrate(VerletStep,"verlet.eps")

