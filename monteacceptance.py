#!/usr/bin/env python
#-*- coding:utf-8 -*-
from __future__ import division, print_function
import numpy
import matplotlib.pyplot as plt
import random


Nsteps = 1000000



def runmc(xnew,delta):
  x=0.5
  nacc = 0
  nbins = 25
  bins = numpy.linspace(0,1,nbins+1, endpoint=True)
  dist = numpy.zeros(nbins)
  for jj in xrange(Nsteps):
    assert 0<=x<1
    chi = (random.random() * 2 - 1 )*delta
    xn,accprob = xnew(x,chi)
    if 0<=xn<1  and random.random() < accprob:
      # accept the move
      x = xn
      nacc +=1
    else:
      # reject the move
      pass
    # accumulate dist
    dist[ numpy.logical_and(x<bins[1:],x>=bins[:-1]) ] +=1
  accratio = float(nacc) / Nsteps
  dist = dist / Nsteps / (bins[1]-bins[0])
  return accratio, dist, bins
    
  
def xnew_std(x,chi):
  accprob = 1
  return x+chi, accprob

def xnew_inv(x,chi):
  phi = 1+chi if chi>=0 else (1-chi)**-1
  xn = x*phi
  accprob = xn/x if xn<x else 1
  return xn, accprob

def xnew_invwrong(x,chi):
  phi = 1+chi if chi>=0 else (1-chi)**-1
  xn = x*phi
  accprob = 1
  return xn, accprob

accratio, dist, bins = runmc(xnew_std, delta=0.5)
print (accratio)
plt.figure()
plt.bar(bins[:-1],dist,bins[1]-bins[0])
plt.xlim((-0.5,1.5))
plt.savefig("monteacceptatnce_std.eps")

accratio, dist, bins = runmc(xnew_inv, delta=3)
print (accratio)
plt.figure()
plt.bar(bins[:-1],dist,bins[1]-bins[0])
plt.xlim((-0.5,1.5))
plt.savefig("monteacceptatnce_inv.eps")

accratio, dist, bins = runmc(xnew_invwrong, delta=3)
print (accratio)
plt.figure()
plt.bar(bins[:-1],dist,bins[1]-bins[0])
plt.xlim((-0.5,1.5))
plt.savefig("monteacceptatnce_invwrong.eps")


