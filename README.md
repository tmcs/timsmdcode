Tim's methods of computer simulation assessment. "assessment" branch.


symplectic.py
=============
A comparison of some symplectic and non-symplectic integrators for integrating
Newtons equations of motion for a harmonic oscillator. Makes some phase space
plots.

monteacceptance.py
==================
A monte carlo simulation of a particle in a box with two different acceptance
criteria

nosehoover.py
=============
A (currently unimplemented) Nose-Hoover thermostat

lennardjones/*.py
=================
Programs to run Lennard Jones monte carlo simulations and produce graphs

