#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""Class for general monte carlo simulation"""


from __future__ import division, print_function
import numpy
import simulation
import random


class MonteCarlo (simulation.Simulation):
  """
  Run a monte-carlo simulation.

  Usage:
  >>> mcsim = montecarlo.MonteCarlo(simulation parameters...)
  >>> mcsim.run(numberofsteps)

  """


  def __init__(self,atompos, boxsize, cutoff , stepfrac, 
            volmovesize=None,pressure=None, getreactioncoord=None, **kwargs ):
    """lengths are measured in sigma. Set `stepfrac` to make the acceptance
   ratio sensible. For constant-pressure simulations, specify `pressure` and
    `volmovesize`. 
    """
    self.pressure = pressure
    self.volmovesize = volmovesize
    self.stepfrac = stepfrac

    simulation.Simulation.__init__(self, atompos, boxsize, cutoff, **kwargs)

    # initialise the energy contributions from each atom
    self.potenergycontribs = self.calcpotenergycontribs()

    self.nmoves = 0
    self.nvolmoves = 0
    self.nacc = 0
    self.nvolacc = 0

    self.getreactioncoord = getreactioncoord


###########################################################################
###########################################################################
###########################################################################


  def makemove(self):
    atompos = self.atompos
    stepfrac = self.stepfrac
    temperature = self.temperature 
    boxsize = self.boxsize
    stepsize = boxsize*stepfrac

    N,d = atompos.shape

    # choose an atom
    atomno = random.choice(xrange(N))      

    # move the atom
    origpos = atompos[atomno]
    newpos = (origpos + 2*stepfrac*(
                              numpy.random.rand(d)-0.5
                                    ))  % boxsize

    # Find change in energy
    oldcontrib = self.potenergycontribs[atomno]
    assert not numpy.isinf(oldcontrib)
    newcontrib = self.calcpotenergy_oneatom( newpos,atomno)
    deltaE = newcontrib - oldcontrib

    # accept or reject move
    if deltaE<0 or random.random()<numpy.exp(-deltaE/temperature):
      # acept the move
      self.nacc += 1
      atompos[atomno] = newpos
      self.potenergycontribs[atomno] = newcontrib
      moveaccepted = True
    else:
      # reject the move
      moveaccepted = False 
    self.nmoves +=1

    return moveaccepted

###########################################################################
###########################################################################
###########################################################################


  def getaccratio(self):
    return self.nacc / self.nmoves

###########################################################################
###########################################################################
###########################################################################

  def getvolaccratio(self):
    return self.nvolacc / self.nvolmoves

###########################################################################
###########################################################################
###########################################################################

  def makevolumemove(self):
    atompos, volmovesize = self.atompos, self.volmovesize
    temperature = self.temperature
    pressure = self.pressure
    N,d = self.atompos.shape
    origbox = self.boxsize

    origvol = origbox**d

    # ------------- generate volume step ---------------------------------
    deltav = random.random()*2*self.volmovesize - self.volmovesize
    newvol = origvol+deltav
    # ----------end generate volume step ---------------------------------

    if newvol>0:
      phi = (1 + deltav / origvol)**(1./d)
      

      # ------------- move atoms and scale box size ----------------------
      Econtribold = self.potenergycontribs 
      self.atompos *= phi
      self.boxsize *= phi
      Econtribnew = self.calcpotenergycontribs()
      Eorig = Econtribold.sum()*0.5
      assert not numpy.isinf(Eorig)
      Enew  = Econtribnew.sum()*0.5
      # ----------end move atoms and scale box size ----------------------

      # --------------- find acceptance probability ----------------------
      GG = (newvol / origvol)**N  \
                * numpy.exp(-1/temperature * pressure * (newvol-origvol)) \
                * numpy.exp(-1/temperature * (Enew - Eorig) )

      accprob = min(1, GG)
      # ------------end find acceptance probability ----------------------

      if random.random()<accprob:
        # -------------- acept the move ----------------------------------
        self.nvolacc +=1
        moveaccepted = True
        # -----------end acept the move ----------------------------------
      else:
        # -------------- reject the move ----------------------------------
        self.simcell.atompos /= phi
        self.simcell.boxsize /= phi
        self.poten = Eorig 
        moveaccepted = False
        # -----------end reject the move ----------------------------------
        
    else:
      print ("WARNING: boxsize less than twice cutoff")
      # reject move if newvol<=0
      moveaccepted = False

    self.nvolmoves += 1
    
    return moveaccepted 

###########################################################################
###########################################################################
###########################################################################

  def run(self, mcsteps,fnsuffix=None, pressureevery=None, volmoveevery=None,
          gaussianevery=None, reactcoordoutfile=None, reactcoordevery=None):

    N,d = self.atompos.shape

    if pressureevery is not None:
      self.pressures = numpy.zeros(mcsteps)
      self.pressures[:] = numpy.nan # pressures is nan if not calculated

    # ----------------- Write energies as we go along to text file -----------
    if fnsuffix is not None: 
      print ("Running simulation suffix: ", fnsuffix)
      energyfile = open("energiesmc"+fnsuffix+".csv","w")
    else:
      energyfile = None
    # --------------end Write energies as we go along to text file -----------

    # ---------------- Write gaussian locations as we go along ---------------
    if reactcoordoutfile is not None:
      reactcoordoutfile = open(reactcoordoutfile, "w")
    # -------------end Write gaussian locations as we go along ---------------

    try:
      if energyfile is not None:
        print("acc_ratio energy volaccratio boxsize", file=energyfile)
      for stepnum in xrange(mcsteps):
        for movenum in xrange(N): # acceptance ratio is about 0.1
          # ---------------------- Monte Carlo step -------------------------------
          self.makemove()
          # -------------------end Monte Carlo step -------------------------------
          
        # --------------------- thermo quantities -------------------------------
        if energyfile is not None:
          potenergy = self.potenergycontribs.sum()*0.5
          if self.getreactioncoord is not None:
            loc = self.getreactioncoord(self)
          else:
            loc=numpy.nan
          print (numpy.nan,potenergy,numpy.nan,self.boxsize, loc,file=energyfile)
        # ------------------end thermo quantities -------------------------------

        # --------------------- volume moves ------------------------------------
        if volmoveevery is not None and  stepnum % volmoveevery ==0:
          mc.makevolumemove()
        # ------------------end volume moves ------------------------------------

        # --------------------- Calculate pressure ------------------------------
        if pressureevery is not None and stepnum % pressureevery==0:
          self.pressures[stepnum] = self.getvirialpressure()
        # ------------------end Calculate pressure ------------------------------

        # --------------------- Lay gaussian ------------------------------
        if gaussianevery is not None and stepnum % gaussianevery==0:
          loc = self.getreactioncoord(self)
          self.biaspot.putgaussian(loc) 
          numpy.save("biaspot.npy",self.biaspot._pot)
          # ................. Recalculate the energy .....................
          biasedatoms = numpy.any(self.biaspairs, axis=1)
          biasedatoms = biasedatoms.nonzero()[0]
          for atomno in biasedatoms:
            oneatompos = self.atompos[atomno]
            newcontrib = self.calcpotenergy_oneatom( oneatompos,atomno)
            self.potenergycontribs[atomno] = newcontrib
          # ..............end Recalculate the energy .....................
        # ------------------end Lay gaussian  ------------------------------

        # ----------------- output reaction coord value --------------------
        if reactcoordevery is not None and stepnum % reactcoordevery==0:
          loc = self.getreactioncoord(self)
          if reactcoordoutfile is not None: print (loc, file=reactcoordoutfile)
        # --------------end output reaction coord value --------------------

      print ("acc ratio", self.getaccratio())

    finally:
      if energyfile is not None:  energyfile.close()
      if reactcoordoutfile is not None: reactcoordoutfile.close()

###########################################################################
###########################################################################
###########################################################################

  def getaveragepressure(self):
    pressures = self.pressures
    # the nan values were not calculated
    pressures = pressures[numpy.logical_not(numpy.isnan(pressures))]
    assert len(pressures)
    avpress = pressures.sum()/len(pressures)
    stderrpress = ( ((pressures-avpress)**2).sum()/len(pressures) 
                            )**0.5  / len(pressures)**0.5 
    return avpress,stderrpress

#==========================================================================
#==========================================================================
#==========================================================================




  
  





