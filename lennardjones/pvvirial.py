#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Make a P against rho graph

Usage:
./pvvirial.py 
"""

from __future__ import division, print_function
import numpy
import matplotlib.pyplot as plt

import montecarlo

import initial
##########################################################################
##########################################################################
##########################################################################

def runpvvirial():
  
  
  rhos = numpy.linspace(0.1,0.9,10)
  equilsteps = 600
  runsteps = 300
  pressureevery = 10
  stepfrac = 0.03
  hardcore = 0
  cutoff = 2
  d=3
  n_cells=3

  temperatures = numpy.array([1.5,2.0,2.5])
  ntemp = len(temperatures)
  nrho = len(rhos)
  pressures = numpy.zeros( (ntemp,nrho ),dtype=float)
  stderrpressures = numpy.zeros( (ntemp,nrho ),dtype=float)

  for jj, temperature in enumerate(temperatures):
    for ii, rho in enumerate(rhos):
      print ("Trying point", jj, ii, "rho=",rho, "temperature=",temperature )
      fnsuffix = "_temp{:03}_rho{:03}".format(jj,ii)

      # ----------------- Equilibrate ------------------------------------
      atompos, boxsize = initial.fccconfig(d,n_cells,rho=rho)
      equil = montecarlo.MonteCarlo(atompos, boxsize, cutoff , stepfrac, temperature=temperature,
                                                                hardcore=0)
      equil.run(equilsteps)
      # --------------end Equilibrate ------------------------------------

      # --------------- Run simulation -----------------------------------
      atompos, boxsize = equil.atompos, equil.boxsize
      mcsim = montecarlo.MonteCarlo(atompos, boxsize, cutoff , stepfrac, temperature=temperature  ,
                                                                hardcore=0)
      mcsim.run(runsteps, fnsuffix=fnsuffix, pressureevery=pressureevery )

      avpress,stderrpress = mcsim.getaveragepressure()

      pressures[jj,ii] = avpress
      stderrpressures[jj,ii] = stderrpress
      # ------------end Run simulation -----------------------------------

      # ----------------- Write out final -------------------------------
      numpy.save("atompos"+fnsuffix+".npy", atompos)
      numpy.save("boxsize"+fnsuffix+".npy", boxsize)
      # --------------end Write out final -------------------------------
  
  numpy.save("pressures.npy", pressures)
  numpy.save("stderrpressures.npy", stderrpressures)
  numpy.save("rhos.npy", rhos)
  numpy.save("temperatures.npy", temperatures)

##########################################################################
##########################################################################
##########################################################################
def getjohnson(cutoff):
  # cut and shifted correciton
  rhojohnson = numpy.array([1,2,4,5,6,7,8,9])*0.1
  pcs = -32/9.*numpy.pi*rhojohnson**2*(1/cutoff**9  -3/2.*1/cutoff**3)

  pjohnson = """0.1777    
  0.3290
  0.705
  1.069
  1.756
  3.024
  5.28  
  9.09"""
  pjohnson = numpy.array(pjohnson.splitlines(),dtype=float)
  stderrjohnson = """2e-4
  6e-4
  1e-3
  3e-3
  7e-3
  7e-3
  1e-2
  2e-2"""
  stderrjohnson = numpy.array(stderrjohnson.splitlines(),dtype=float)
  pjohnson += pcs  
  return rhojohnson, pjohnson, stderrjohnson

def loadpvvirial():

  cutoff = 2

  pressures = numpy.load("pressures.npy")
  temperatures = numpy.load("temperatures.npy")
  stderrpressures = numpy.load("stderrpressures.npy")
  rhos = numpy.load("rhos.npy")
  for jj, temp in enumerate(temperatures):
    plt.errorbar( rhos ,pressures[jj] , yerr=stderrpressures[jj], 
                  label="$T={}$".format(temp))

  rhojohnson, pjohnson, stderrjohnson = getjohnson(cutoff)
    
  plt.errorbar( rhojohnson, pjohnson, yerr=stderrjohnson, label="$P_J, T=2.0$" )
  plt.legend(loc='upper left')
  plt.xlim((0,1))
  yl = plt.ylim()
  plt.ylim((0,yl[1]))
  plt.xlabel(r"$\rho/\sigma^{-3}$")
  plt.ylabel(r"$p/\epsilon\cdot\sigma^{-2}$")
  plt.savefig(r"pvvirial.eps")

##########################################################################
##########################################################################
##########################################################################

if __name__=="__main__":
  #runpvvirial()
  loadpvvirial()
    

