#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Simulation base class.
"""


from __future__ import division, print_function
import numpy
import position


# =========================================================================
# =========================================================================
# =========================================================================

class Simulation (object):
  """
  Simulation base class. This is a convenience class and is meant to be 
  subclassed.
  """

  def __init__(self, atompos, boxsize, cutoff, temperature=None, hardcore=0,
           biaspot=None, biaspairs=None):

    self.cutoff = cutoff
    self.temperature = temperature
    self.hardcore = hardcore
    self.biaspot = biaspot 
    self.biaspairs = biaspairs

    self.boxsize = boxsize  
    self.atompos = atompos

    N,d = self.atompos.shape

    if self.biaspairs is None:
      self.biaspairs = numpy.zeros((N,N), dtype=bool)


  
###########################################################################
###########################################################################
###########################################################################

  @property
  def boxsize(self):
    return self._boxsize
  @boxsize.setter
  def boxsize(self,val):
    assert val>self.getminboxsize()-1.e-7, \
      ("box size should be larger than ",val ,self.getminboxsize())
    self._boxsize = val

  @property
  def atompos(self):
    return self._atompos
  @atompos.setter
  def atompos(self, val):
    self._atompos = val
    atompos = self._atompos
    assert self.checkhardcoreseps()
    assert self.checkallinbox(), (
      "Some atoms are outside the box!", atompos.max(),atompos.min(), boxsize)

###########################################################################
###########################################################################
###########################################################################

  def checkallinbox(self):
    atompos = self.atompos
    boxsize = self.boxsize
    configok = ( atompos.max()<boxsize and atompos.min()>=0 )
    return configok
      
###########################################################################
###########################################################################
###########################################################################

  def getminboxsize(self):
    return self.cutoff*2 

###########################################################################
###########################################################################
###########################################################################

  def getpotential(self, r, biaspairs=None):
    cutoff = self.cutoff
    hardcore = self.hardcore
    biaspot = self.biaspot

    return position.getpotential( r, cutoff, hardcore,  
                                    biaspot=biaspot, biaspairs=biaspairs)

###########################################################################
###########################################################################
###########################################################################

  def getscalarforces(self, modseps):

    cutoff = self.cutoff
    hardcore = self.hardcore    
    biaspot = self.biaspot

    return position.getscalarforces(modseps, cutoff, hardcore, biaspot=biaspot)

###########################################################################
###########################################################################
###########################################################################

  def getvirialpressure(self):
    atompos = self.atompos
    boxsize = self.boxsize
    cutoff = self.cutoff
    hardcore = self.hardcore
    temperature = self.temperature

    return position.getvirialpressure(atompos, boxsize, cutoff, hardcore, 
                                                      temperature)

###########################################################################
###########################################################################
###########################################################################

  def checkhardcoreseps(self):
    """Returns true if configuration is ok"""
    atompos = self.atompos
    boxsize = self.boxsize
    hardcore = self.hardcore
    return position.checkhardcoreseps(atompos, boxsize,hardcore )

###########################################################################
###########################################################################
###########################################################################

  def calcseps(self):
    atompos, boxsize = self.atompos, self.boxsize
    return position.calcseps(atompos, boxsize)

###########################################################################
###########################################################################
###########################################################################

  def calcseps_oneatom( self, oneatompos):
    atompos, boxsize = self.atompos, self.boxsize
    return position.calcseps_oneatom(atompos, boxsize, oneatompos)

###########################################################################
###########################################################################
###########################################################################

  def calcpotenergy_oneatom(self, oneatompos,atomno):
    atompos,cutoff, boxsize = self.atompos,self.cutoff, self.boxsize
    hardcore = self.hardcore
    biaspot = self.biaspot
    biaspairs = self.biaspairs
    return position.calcpotenergy_oneatom(atompos,boxsize, cutoff, hardcore,
                                oneatompos,atomno, 
                      biaspot=biaspot, biaspairs=biaspairs)


###########################################################################
###########################################################################
###########################################################################

  def calcpotenergycontribs(self):
    atompos,cutoff, boxsize = self.atompos,self.cutoff, self.boxsize
    biaspot = self.biaspot
    biaspairs = self.biaspairs
    hardcore = self.hardcore
    return position.calcpotenergycontribs(
                atompos,cutoff, boxsize, hardcore, 
                  biaspot=biaspot, biaspairs=biaspairs)


###########################################################################
###########################################################################
###########################################################################

  def calcpotenergy(self):

    potenergycontribs = self.calcpotenergycontribs()
    return 0.5*calcpotenergycontribs.sum() # avoid double counting with 0.5
  

#==========================================================================
#==========================================================================
#==========================================================================


