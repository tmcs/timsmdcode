#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function
import numpy
from itertools import product


############################################################################
############################################################################
############################################################################
def getrstart(rho, d):
  atomspercell = d+1
  rstart = 2**-0.5*(atomspercell/rho)**(1./d)
  return rstart

############################################################################
############################################################################
############################################################################
def fccconfig(d,n_cells,rstart=None, rho=None):
  """n_cells is the number of cells in each direction.
  `rstart` is the distance between atoms"""


  if rstart is None:
    assert rho is not None
    rstart =  getrstart(rho, d)

  # r is the radius of an imaginary sphere
  r = rstart/2.

  # number of atoms. There are d+1 per unit cell.
  N=(d+1)*n_cells**d
  assert N<1.e6, ("TOO MANY ATOMS",n)

  # unit cell lattice parameter
  l=r*2*(2**(0.5))

  # create a list of zeros.
  atompos=[[0]*d]*N

  for i in range (1,d+1):
    atompos[i]=[l/2.]*d
    atompos[i][i-1]=0

  assert N%(d+1)==0

  i=0

  for indlist in product(range(0,n_cells), repeat = d):
    for j in range(0,d+1):
      atompos[i]=[atompos[j][dim]+indlist[dim]*l for dim in range(d)]
      i=i+1

  atompos = numpy.array(atompos)
  assert atompos.shape == (N,d)
  boxsize = n_cells*l

  assert numpy.all( numpy.logical_and(
                  atompos>=0, atompos<boxsize))

  return atompos, boxsize

