#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Use this script to watch a monte carlo simulation converge as it is running
Usage:
plotconvergence.py energiesmc_NAME.csv
"""


from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy
import sys

lastn = 10000  

if __name__=="__main__":
  

  lines = open(sys.argv[1]).readlines()
  lines = [line.split() for line in lines[1:] if len(line.strip())]
  lines = [line for line in lines if len(line)==5]
  lines = numpy.array(lines)
  energy, boxsize,reactcoord = \
            lines[:,1], lines[:,3], lines[:,4]
  energy = energy.astype(float)
  boxsize = boxsize.astype(float)
  reactcoord = reactcoord.astype(float)


  plt.figure()
  plt.plot(reactcoord)
  plt.title("react coord")
  
  plt.figure()
  plt.plot(energy)
  plt.title("energy")

  plt.figure()  
  plt.hist(reactcoord[-lastn:], bins=10)

  plt.figure()
  originalpotential = numpy.load("originalpotential.npy")
  rr = numpy.load("biasrr.npy")
  bias = numpy.load("biaspot.npy")
  sumpot = originalpotential+bias
  #originalpotential -= originalpotential[0]
  #bias -= bias[0]
  #sumpot -= sumpot[0]
  plt.plot( rr, originalpotential , label="original" )
  plt.plot(rr, bias, label="bias")
  plt.plot(rr,sumpot , label="original+bias")
  plt.legend()

  plt.show()




