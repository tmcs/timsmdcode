#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Run a metadynamics MonteCarlo simulation. 

Usage:
./metadynamics.py [--reload] [--test]

Use --reload to continue a previous calculation.
Use --test to test a biasing potential
"""


from __future__ import division, print_function
import montecarlo
import numpy
import initial
import biaspotential
import position




def runmetadynamics(reloadall=False, test=False):
  n_cells = 3 # per direction
  d=3
  rho=0.8
  hardcore = 0
  equilsteps = 800
  runsteps = 10000 
  teststeps = 10000

  cutoff = 2
  stepfrac = 0.11
  biasmin = 1
  biasmax = 2
  biaspotpoints = 200
  gaussianevery = 100
  gaussianwidth = 0.4 
  gaussianheight = 2
  temperature = 2
  reactcoordevery = 10
  reactcoordoutfile = "reactcoords.csv"
  #numgrowthsteps = runsteps / gaussianevery *4
  #gaussiangrowth = ((biasmax-biasmin)-gaussianwidth) / numgrowthsteps
  gaussiangrowth=0
  # ----------------- load initial config ----------------------------
  if not reloadall:
    atompos,boxsize = initial.fccconfig(d,n_cells,rho=rho)
  else:
    atompos = numpy.load("finalatompos.npy")
    boxsize = float(numpy.load("finalboxsize.npy"))
  # ------------- end load initial config ----------------------------

  N,d = atompos.shape


  # ---------------- Load bias potential ------------------------------
  if not reloadall and not test:
    pot = numpy.zeros(biaspotpoints, dtype=float)
  else:
    pot = numpy.load("biaspot.npy")
  biaspot = biaspotential.BiasPotential(biasmin,biasmax, biaspotpoints, pot,
                                         gaussianwidth=gaussianwidth,
                                          gaussianheight=gaussianheight)
  # ---------------- Load bias potential ------------------------------
  
  # -------------- Output the original potential ----------------------
  rr = biaspot.rr
  numpy.save("biasrr.npy", rr)
  originalpotential = position.getpotential(rr, cutoff, hardcore)
  numpy.save("originalpotential.npy", originalpotential)
  # -----------end Output the original potential ----------------------

  # -------------- Which pair of atoms do we care about? -----------
  biaspairs = numpy.zeros((N,N),dtype=bool)
  biaspairs[0,1] = True
  biaspairs[1,0] = True
  # ----------  end Which pair of atoms do we care about? -----------


  # ------------------ Equilibrate ----------------------------------
  if not reloadall:
    print ("equilbrating")
    equil = montecarlo.MonteCarlo(atompos, boxsize, cutoff , stepfrac, 
                                                    temperature=temperature,
                                                    biaspot=biaspot,
                                                    biaspairs=biaspairs)
    equil.run(equilsteps)
  # ------------------ Equilibrate ----------------------------------

  def getreactioncoord(sim): # the distance between atoms 0 and 1
    atom1pos = sim.atompos[0]
    dist2 = sim.calcseps_oneatom(atom1pos)[1]
    dist2 = (dist2**2).sum()**0.5
    return dist2

  # ------------------ Run the simulation ---------------------------
  if not test:
    print ("running")
    mcsim =  montecarlo.MonteCarlo(atompos, boxsize, cutoff , stepfrac, 
                                                    temperature=temperature,
                                                    biaspot=biaspot,
                                                    biaspairs=biaspairs,
                                     getreactioncoord=getreactioncoord     )

    fnsuffix = "_gr"
    mcsim.run(runsteps, fnsuffix, gaussianevery=gaussianevery,
              reactcoordoutfile=reactcoordoutfile, reactcoordevery=reactcoordevery)
  # -------------- end Run the simulation ---------------------------

    numpy.save("finalatompos.npy", mcsim.atompos)
    numpy.save("finalboxsize.npy", mcsim.boxsize)

  # -------------------- Test the distribution ---------------------
  else:
    testsim =  montecarlo.MonteCarlo(atompos, boxsize, cutoff , stepfrac, 
                                                    temperature=temperature,
                                                    biaspot=biaspot,
                                                    biaspairs=biaspairs,
                                     getreactioncoord=getreactioncoord     )

    fnsuffix = "_testsim"
    testsim.run(teststeps, fnsuffix, gaussianevery=None,
              reactcoordoutfile=reactcoordoutfile, reactcoordevery=reactcoordevery)  
  # -------------------- Test the distribution ---------------------




if __name__=="__main__":
  reloadall = False
  test=False
  import sys
  if len(sys.argv)>=2:
    if sys.argv[1]=="--reload":
      reloadall=True
    elif sys.argv[1]=="--test":
      test=True
    else:
      print ("Unrecognised argment")
      sys.exit(1)

  runmetadynamics(reloadall=reloadall, test=test)


