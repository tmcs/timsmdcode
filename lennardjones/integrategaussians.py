#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Demonstration of the edge correction to the bias potential for metadynamics.
N gaussians are added together in the range [a,b] and plotted. The correction
term comes from the tails of a set of "imaginary" gaussians from outside the 
range.

Usage:
./integrategaussians.py
"""

from __future__ import division, print_function

import numpy
import scipy.special
import matplotlib.pyplot as plt

if __name__=="__main__":

  a=1
  b=2
  sigma = 0.1
  A=1
  N = 30
  n = N/(b-a)
  x = numpy.linspace(0.5, 2.5,100)
  gaussianarea = A*numpy.pi**0.5*sigma
  correction = N/(b-a)*gaussianarea*(
                1 - 0.5*scipy.special.erf((x-a)/sigma)+0.5*scipy.special.erf((x-b)/sigma)
                                        )

  gaussians = numpy.linspace(a,b,N, endpoint=False)
  gaussians += (gaussians[1]-gaussians[0])*0.5 # shift them all along half a spacing  
  gaussiansum = (A*numpy.exp(-((x[:,None]-gaussians[None,:])/sigma)**2)).sum(axis=1)


  plt.plot(x,correction)
  plt.plot(x, gaussiansum)
  plt.plot(x,gaussiansum+correction)
  plt.xlim((a,b))
  plt.show()
                           
