#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function
import numpy


class Raddistfn(object):
  def __init__(self, pairs, sepbins):
    self.sepbins = sepbins 
    nbins = len(self.sepbins)-1
    self.numinsepbin = numpy.zeros(nbins)
    self.naccumulatepairs = 0

###########################################################################
###########################################################################
###########################################################################

  def accumulate(self, atompos, boxsize):
    assert self.sepbins is not None, "Need to specify some sepbins"
    N,d = atompos.shape
    seps = position.calcseps(atompos, boxsize) # NxNxd array
    modseps = (seps**2).sum(axis=2)**0.5
    diags = numpy.eye(N,dtype=bool)
    pairs = self.pairs
    assert not numpy.any( numpy.logical_and(diags, pairs) ), \
        "pairs should not contain any diagonal terms."

    # --------------------- Only include some pairs -------------------
    modseps[numpy.logical_not(pairs)]=-1
    # --------------------- Only include  some pairs-------------------

    nbins = len(sepbins)-1
    # number of pairs in each separation bin
    thisnuminsepbin = ( 
          numpy.logical_and(
              modseps[:,:,None] > sepbins[None,None,:-1],
              modseps[:,:,None] < sepbins[None,None,1:]
                )
            ).sum(axis=(0,1)) // 2 # 2 avoids double counting
    assert thisnuminsepbin.shape==(nbins,)
    self.numinsepbin += thisnuminsepbin
    Npairs = self.pairs.sum()//2 # N(N-1)/2
    self.naccumulatepairs += Npairs  # total number of accumulated pairs
    

###########################################################################
###########################################################################
###########################################################################

  def getraddist(self):

    N, d = self.atompos.shape
    sepbins = self.sepbins
    boxsize = self.boxsize

    # find the pdf
    pdf = self.getpdf()

    raddist = pdftoraddist(pdf, sepbins,N,d, boxsize )

    return raddist
  

###########################################################################
###########################################################################
###########################################################################

  def getpdf(self):
    
    assert self.naccumulatepairs >0, "No separation data accumulated"

    # fraction of pairs with each separation
    pdf = self.numinsepbin / self.naccumulatepairs

    return pdf
#==========================================================================
#==========================================================================
#==========================================================================



##########################################################################
##########################################################################
##########################################################################

def pdftoraddist(pdf, sepbins,N,d, boxsize ):

  nbins = len(sepbins)-1

  # get the separation at middle of each bin
  rr = (sepbins[1:]+sepbins[:-1])/2.
  
  # find bin widths
  binwidths = sepbins[1:]-sepbins[:-1]

  # get surface area of ball in d dimensions
  surfarea =  jacobian(d) * d * rr**(d-1)

  # find density:
  rho = N/(boxsize**d)

  # bin volume. eg 4*pi*r**2*dr
  binvol = surfarea*binwidths 

  raddist = pdf*N/(rho*binvol) # actual / expected

  assert raddist.shape == (nbins,)

  return raddist 

