#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function
import numpy
import scipy.special

def calcseps(atompos, boxsize):
  """Returns the separation vectors r_ij according to minimum image 
  convention"""

  N,d = atompos.shape

  # an NxNxd array
  seps = atompos[None,:,:] - atompos[:,None,:]

  # minimum image convention
  seps[seps >  boxsize/2.] -= boxsize
  seps[seps < -boxsize/2.] += boxsize

  assert seps.shape==(N,N,d)

  return seps

###########################################################################
###########################################################################
###########################################################################

def calcseps_oneatom( atompos, boxsize, oneatompos):
  """Returns the separation vectors r_ij according to minimum image 
  convention"""

  N,d = atompos.shape
  assert oneatompos.shape==(d,)

  # an Nxd array
  seps = atompos - oneatompos

  # minimum image convention
  seps[seps >  boxsize/2.] -= boxsize
  seps[seps < -boxsize/2.] += boxsize

  assert seps.shape==(N,d)

  return seps

###########################################################################
###########################################################################
###########################################################################

def getpotential( r, cutoff, hardcore, biaspot=None, biaspairs=None):
  """r is a numpy array. Pass in 1 to get rid of annoying divide-by-zero 
  errors"""

  def v_(r):
    vr =  4*( r**-12 - r**-6 ) 
    return vr

  if cutoff>0:
    Vee = v_(r) - v_(cutoff)
    Vee[r>cutoff] = 0 # zero outside cutoff
  else:
    Vee = 0*r

  # ------------------------ Add on bias pot -----------------------
  if biaspot is not None:
    if biaspairs is None:
      raise ValueError( "You must specify which pairs to add bias for")
    biaspotvals =  biaspot.getbiaspot(r)
    biaspotvals[numpy.logical_not(biaspairs)] = 0
    Vee += biaspotvals
  # ------------------------ Add on bias pot -----------------------

  # ------------------------ Add on hard core --------------------------
  Vee[r<=hardcore] = +numpy.inf
  # ------------------------ Add on hard core --------------------------

  # negative infinite potentials not supported. Positive are.
  assert not numpy.any( numpy.isneginf(Vee) )

  return Vee

###########################################################################
###########################################################################
###########################################################################
def getscalarforces( modseps, cutoff, hardcore, biaspot=None):

  N,N = modseps.shape

  
  # ---------------- The Lennard jones forces -----------------------------
  def vprime_(r):
    vprimer= 4*( -12*r**-13 - -6*r**-7 )
    return vprimer
  # -------------end The Lennard jones forces -----------------------------

  # ---------------------- Lennard jones cutoff ---------------------------
  if cutoff>0:
    scalarforces = vprime_(modseps)
    scalarforces[modseps>cutoff] = 0 # force zero outside cutoff
  else:
    scalarforces = 0*modseps
  # -------------------end Lennard jones cutoff ---------------------------

  # ------------------ Hard core --------------------------------------
  # do nothing. This should be handled by the calling function
  # ---------------end Hard core --------------------------------------
    
  # ---------------- Umbrella pot -------------------------------------
  if biaspot is not None:
    raise NotImplementedError("bias forces not implemented")
  # -------------end Umbrella pot -------------------------------------

  return scalarforces

###########################################################################
###########################################################################
###########################################################################


def getvirialpressure(atompos, boxsize, cutoff, hardcore, temperature):
  N,d = atompos.shape

  density = N/boxsize**d

  seps = calcseps(atompos, boxsize)
  modseps = (seps**2).sum(axis=2)**0.5
  diags = numpy.eye(N, dtype=bool)
  modseps[diags] = 1 # supress annoying messages
  scalarforces = getscalarforces(modseps, cutoff, hardcore)
  scalarforces[diags] = 0 # i=j terms

  virial = 1./d*0.5*(-modseps*scalarforces).sum()
  assert not numpy.isnan(virial)
  pressure = temperature*density + virial/boxsize**d
  return pressure
###########################################################################
###########################################################################
###########################################################################

def checkhardcoreseps(atompos, boxsize,hardcore ):
  """Returns true if configuration is ok"""
  N,d = atompos.shape
  seps = calcseps(atompos, boxsize)
  modseps = (seps**2).sum(axis=2)**0.5
  iequalj = numpy.eye(N, dtype=bool)
  configok = numpy.all( numpy.logical_or(modseps >=hardcore , iequalj)   )
  return configok


###########################################################################
###########################################################################
###########################################################################
def calcpotenergycontribs(atompos,cutoff, boxsize, hardcore, 
                  biaspot=None, biaspairs=None):
  """Find the potential energy of a configuration"""

  N,d = atompos.shape

  seps = calcseps(atompos, boxsize)
  modseps = (seps**2).sum(axis=2)**0.5

  diags = numpy.eye(N, dtype=bool)
  modseps [diags]=1 # suppress warning messages
  Vee = getpotential( modseps, cutoff, hardcore, biaspot=biaspot, 
                          biaspairs=biaspairs)
  Vee[diags] = 0 # set i=j parts to 0:

  potencontribs = Vee.sum(axis=1)

  return potencontribs


###########################################################################
###########################################################################
###########################################################################

def calcpotenergy_oneatom(atompos,boxsize, cutoff,  hardcore,
                                oneatompos,atomno, 
                      biaspot=None, biaspairs=None):
  """Find the contribution to pot en of one atom"""

  seps = calcseps_oneatom(atompos,boxsize, oneatompos)
  modseps = (seps**2).sum(axis=1)**0.5

  # the 0.5 is to avoid double counting
  modseps[atomno] = 1 # annoying messages
  Vee = getpotential( modseps, cutoff, hardcore, biaspot=biaspot, 
                          biaspairs=biaspairs[atomno] )
  Vee[atomno] = 0 # set i=j parts to 0:
  Vee =  Vee.sum() 

  return Vee

###########################################################################
###########################################################################
###########################################################################
def getffatpoint( r, ff, sepbins, valoutside=1):
  """r is a numpy array. Return the value of ff in that bin."""
  sepbinsupper = sepbins[1:] 
  naxes = len(r.shape)
  allaxes = (slice(len(sepbins)) ,) + (None,)*naxes

  # find the first bin that r is less than. 
  binidx = numpy.argmax(  r[None,...] < sepbinsupper[allaxes], axis=0)
  fvals = ff[binidx]

  # set to valoutside outside bins. 
  fvals[ r>=sepbins[-1] ] = valoutside
  fvals[ r<sepbins[0] ] = valoutside

  return fvals

###########################################################################
###########################################################################
###########################################################################

def jacobian(d):
  """eg 4pi in 3d"""
  return numpy.pi**(d/2.)/scipy.special.gamma(d/2.+1)

###########################################################################
###########################################################################
###########################################################################
def calc_spherevol(d, r):

  return numpy.pi**(d/2.0)/scipy.special.gamma((d/2.0)+1.0)*(r**d)
###########################################################################
###########################################################################
###########################################################################
def calc_packfrac(d, N, r, V):
  return N*(calc_spherevol(d,r))/float(V)
###########################################################################
###########################################################################
###########################################################################

def xyzoutput(atompos, fn):
  N,d = atompos.shape
  with open(fn,"w") as f:
    print(N,file=f)
    print(file=f)
    for i in range(N):
      print (1, *[atompos[i,dim] for dim in range(d)], file=f)
###########################################################################
###########################################################################
###########################################################################

