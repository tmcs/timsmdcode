#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Class for metadynamics biasing potential.
"""


from __future__ import division, print_function
import numpy
import scipy.interpolate

class BiasPotential(object):
  """
  A biasing potential made up of gaussians.
  """


  def __init__(self, biasmin, biasmax, biaspotpoints, pot, gaussianwidth,
                gaussianheight, gaussiangrowth=0):
    self._pot = pot
    self.biasmin, self.biasmax = biasmin, biasmax
    self.rr = numpy.linspace(biasmin,biasmax,biaspotpoints)
    self.interp = scipy.interpolate.interp1d(self.rr, self._pot, 
                                                copy=False,
                                                bounds_error=False,
                                                fill_value=numpy.inf)
                                                #assume_sorted=True )
    self.gaussianwidth = gaussianwidth
    self.gaussianheight = gaussianheight
    self.gaussiangrowth = gaussiangrowth

  def getbiaspot(self, r):
    
    biaspot = self.interp(r)
    return biaspot
    

  def putgaussian(self,  loc):
    """Add a Gaussian to the biasing potential. Also adds a correction term at
    the edges of the reaction coordinate range. The location of the Gaussian
    is at loc."""
    width = self.gaussianwidth
    height = self.gaussianheight
    gaussianarea = width * height * numpy.pi**0.5
    gaussianratio = width * height

    rr = self.rr
    gaussian = height * numpy.exp( -( (rr-loc) / width)**2  )
    self._pot[:] += gaussian

    # ------------------ Edge correction ------------------------------
    biasmin, biasmax = self.biasmax , self.biasmin
    reactionboxwidth = biasmax - biasmin
    correction = (
                        1 - 0.5*scipy.special.erf((rr-biasmin)/width)
                          + 0.5*scipy.special.erf((rr-biasmax)/width)
                                      )
    correction = gaussianarea / reactionboxwidth *correction
    self._pot[:] += correction
    # ---------------end Edge correction ------------------------------

    # Move the zero of biasing potential so first element is zero for easy 
    # plotting
    self._pot[:] -= self._pot[0]

    # ------------- make gaussians slightly wider --------------------
    self.gaussianwidth += self.gaussiangrowth
    # preserve their area
    self.gaussianheight = gaussianratio / self.gaussianwidth
    # ----------end make gaussians slightly wider --------------------



